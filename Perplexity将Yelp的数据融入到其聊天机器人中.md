# Perplexity 将 Yelp 的数据融入到其聊天机器人中

>对于你正在考虑的咖啡厅，Perplexity 的回应能够提供来自 Yelp 的多份评价，以及位置数据和其他相关信息。

Perplexity 的执行总裁 Aravind Srinivas 在与 The Verge 的访谈中表示，许多人现在已经习惯于像使用常规搜索引擎一样使用聊天机器人。对于他们来说，最好可以直接从信息源头得到他们想要搜索的信息，例如寻找餐馆等。因此，他们正在努力在人们询问餐馆推荐时，将 Yelp 的地图、评价和其他相关细节集成到回答中。
<br>
Srinivas 解释说：“我们最初的动机是作为一个工具，主要是希望对于需要进行事实查证和研究的人群有吸引力。但是，研究的主题可以包括所有的内容，就比如你是个咖啡爱好者，你肯定会想知道在哪里能找到好喝的咖啡。”<br>
正如 The Verge 的 David Pierce 最近所强调的，Perplexity 在回答问题时混合文字和链接的方式，已经使其相较于 Copilot, Gemini 或 ChatGPT 等其他聊天机器人展现出优势。Srinivas 指出，这也是他们最后选择合作 Yelp 的部分原因。他进一步表示，Yelp 的数据使 Perplexity 能够以多种方式提供答案，如添加地图、照片，甚至引用评论。

![pplx_map_preview_desktop.jpg](https://raw.gitcode.com/lovinpanda/TheRoadtoAI/attachment/uploads/1eb54241-9277-48ca-b4b5-6e7483e11619/pplx_map_preview_desktop.jpg 'pplx_map_preview_desktop.jpg')

这些回应中包含了企业在 Yelp 页面的链接，因此，用户可以直接查看更多评论或其他详细信息。Perplexity 没有透露该交易的详细金融信息，并强调，他们不打算使用 Yelp 的数据来训练任何模型，因为他们在聊天机器人中已经使用了现有的模型，如 GPT 和 Claude 2。

这种合作是 Yelp 的 Fusion 项目的一部分，该项目将 Yelp 的数据授权给第三方，包括宝马公司。Yelp 的业务和公司发展副总裁 Chad Richard 在向 The Verge 发送的电子邮件中表示: "通过将我们稳定且详实的本地数据引入 Perplexity，我们就能为他们的用户提供可靠的本地搜索结果。"

Perplexity 是最新一家与其他实时数据源签订协议的 AI 公司，尽管 Srinivas 强调，他们与 Yelp 的方式并不是合作，而是数据许可。据报道，谷歌将每年向 Reddit 支付6000万美元来获取实时数据，并使用这些数据来训练其 AI模型。OpenAI也已与Axel Springer等新闻出版商签署协议，以获取用于训练的数据。

其他聊天机器人也可以推荐咖啡店和餐馆，但这些数据大部分都源于从互联网上学习来的信息模型，仿佛是机器人替用户执行了搜索功能。有些聊天机器人可以访问一些已经整合了企业信息的服务，因此获得了一定的优势。例如，Google 的 Gemini 在接受了指令后，能提供一份包含营业时间、菜单和地图等详情的餐厅列表，因为它可以从 Google Maps 上获取信息。

Perplexity 计划在不久的未来实现更多这样的集成，并且它已经和答案引擎 WolframAlpha 合作进行数学计算。Srinivas提供了一些可能的例子，例如与购物数据或财务数据的提供者进行合作。

![Mobile.jpg](https://raw.gitcode.com/lovinpanda/TheRoadtoAI/attachment/uploads/8e0de98d-adef-4e36-8d05-82d2237a7c92/Mobile.jpg 'Mobile.jpg')
